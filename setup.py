import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django_email_sms',
    version='1.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'boto',
        'requests',
        'twilio'],
    license='Apache License',
    description='A simple Django app to send emails and sms.',
    long_description=README,
    url='https://bitbucket.org/fafadiatech/django_email_sms',
    author='Yogesh Panchal',
    author_email='yogesh.panchal@fafadiatech.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 1.9',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: python :: 2.7',
        'Programming Language :: Python :: 3',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
