"""
To send email using AWS SES (Simple Email Service)

Add following settings in settings.py

AWS_SES_REGION = ''
AWS_SES_ACCESS_KEY = ''
AWS_SES_SECRET_KEY = ''
"""

from boto import ses
from django.conf import settings
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart


region = settings.AWS_REGION
access_key = settings.AWS_SES_ACCESS_KEY
secret_key = settings.AWS_SES_SECRET_KEY

def send_ses_email(source, subject, body, to_addresses, format='text', attachments=[]):
    """
    send ses email
    """
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = source
    msg['To'] = ", ".join(to_addresses)

    if format == 'text':
        part = MIMEText(body)

    if format == 'html':
        part = MIMEText(body, 'html')

    msg.attach(part)

    if attachments:
        for each_attach in attachments:
            attachment = MIMEApplication(open(each_attach, 'rb').read())
            attachment.add_header('Content-Disposition', 'attachment', filename=each_attach)
            msg.attach(attachment)

    connection = connection = ses.connect_to_region(region, aws_access_key_id=access_key, aws_secret_access_key=secret_key)
    return connection.send_raw_email(msg.as_string(), source=source, destinations=msg['To'])


def test_connection():
    """
    test aws ses connection using given access key and secret key

    if connection succesfull it will return list of verified email addresses
    """
    connection = connection = ses.connect_to_region(region, aws_access_key_id=access_key, aws_secret_access_key=secret_key)
    emails = connection.list_verified_email_addresses()['ListVerifiedEmailAddressesResponse']['ListVerifiedEmailAddressesResult']['VerifiedEmailAddresses']
    return emails


def send_test_email(source, to_addresses):
    """
    send test email using
    """
    send_ses_email(source, 'test email', 'test email', to_addresses, format='text')

