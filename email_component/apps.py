from django.apps import AppConfig


class EmailComponentConfig(AppConfig):
    name = 'email_component'
