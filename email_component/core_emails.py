"""
Django core email function

To use this function add following setting in settings.py

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = (Ex: smtp.gmail.com) (Default='')
EMAIL_PORT = (Ex: 25) (Default=25)
EMAIL_HOST_USER = (Ex: yogesh.panchal@fafadiatech.com) (Default='')
EMAIL_HOST_PASSWORD = (Ex: StrongPasswd)
EMAIL_USE_TLS = (Ex: True or False) (Default=False)
EMAIL_USE_SSL = (Ex: True or False) (Default=False)
EMAIL_TIMEOUT = (Ex: 120) (Default=None)

if you are using SSL connection
EMAIL_SSL_KEYFILE = /path/to/key (Default=None)
EMAIL_SSL_CERTFILE = /path/to/cert (Default=NOne)
"""

from django.core.mail import send_mail
from django.core.mail import get_connection, EmailMultiAlternatives
from django.conf import settings


def send_email(subject, from_email, recipient_list, message, cc=[], html_message=False, attachments=[], connection=None):
    """
    Usage :
    for text email:
        send_email(subject, message, from_email, recipient_email)

    for html email:
        send_email(subject, None, from_email, recipient_list, html_message=html_message)
    """
    if not connection:
        connection = get_connection()

    mail = EmailMultiAlternatives(subject, message, from_email, recipient_list, cc=cc, connection=connection)

    if html_message:
        mail.attach_alternative(message, 'text/html')

    if attachments:
        for attach in attachments:
            mail.attach(filename=attach.name, content=attach.read())

    return mail.send()


def test_connection():
    """
    test smtp connection if connection succeeded returns True
    if connection fails it will raise exception with message
    """
    connection = get_connection()
    return connection.open()


def send_test_email(to=None):
    """
    send test email for testing
    Usage:
    to = ['test@gmail.com']
        send_test_email(to)
    """
    if to:
        subject = 'Test Email'
        message = 'Test Email'
        return send_email(subject, message, settings.EMAIL_HOST_USER, to)
    print ("Usage:\nto = ['test@gmail.com']\nsend_test_email(to)\n")
