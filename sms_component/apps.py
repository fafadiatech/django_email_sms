from django.apps import AppConfig


class SmsComponentConfig(AppConfig):
    name = 'sms_component'
