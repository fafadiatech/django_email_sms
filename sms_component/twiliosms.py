"""
send sms using Twilio sms api

add following settings in settings.py

ACCOUNT_SID = ''
AUTH_TOKEN = ''
FROM = ''
"""

from django.conf import Settings
from twilio.rest import TwilioRestClient


def send_sms(msg, mobile):
    """
    send sms using twilio sms api
    """
    client = TwilioRestClient(settings.ACCOUNT_SID, settings.AUTH_TOKEN)
    return client.sms.messages.create(body=msg, to=mobile, from_=settings.FROM)
