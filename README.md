# A simple Django app to send emails and sms. #

## Installation ##

git clone https://bitbucket.org/fafadiatech/django_email_sms.git

cd django_email_sms/

python setup.py install

or

pip install https://bitbucket.org/fafadiatech/django_email_sms/get/master.zip

## Usage: ##

**Add 'django_email_sms' in your installed_apps**

**(1) To send email using SMTP add following settings in your settings.py**

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = (Ex: smtp.gmail.com) (Default='')

EMAIL_PORT = (Ex: 25) (Default=25)

EMAIL_HOST_USER = (Ex: yogesh.panchal@fafadiatech.com) (Default='')

EMAIL_HOST_PASSWORD = (Ex: StrongPasswd)

EMAIL_USE_TLS = (Ex: True or False) (Default=False)

EMAIL_USE_SSL = (Ex: True or False) (Default=False)

EMAIL_TIMEOUT = (Ex: 120) (Default=None)

if you are using SSL connection

EMAIL_SSL_KEYFILE = /path/to/key (Default=None)

EMAIL_SSL_CERTFILE = /path/to/cert (Default=NOne)


```
#!python
from email_component.core_emails import *

# To test stmp settings
test_connection()

# to send text email
subject = 'subject'
from_email = 'yogesh.panchal@fafadiatech.com'
to = ['test@example.com', 'xyz@example.com']
message = 'My Email Message'

send_email(subject, from_email, recipient_list, message)


# To send HTML email
subject = 'subject'
from_email = 'yogesh.panchal@fafadiatech.com'
to = ['test@example.com', 'xyz@example.com']
message = '<html><body>My HTML Email Message</body></html>'

send_email(subject, from_email, recipient_list, message, html_message=True)



# To send HTML email with attachments
subject = 'subject'
from_email = 'yogesh.panchal@fafadiatech.com'
to = ['test@example.com', 'xyz@example.com']
message = '<html><body>My HTML Email Message</body></html>'
attachments = ['File_Object_1', 'File_Object_2']

send_email(subject, from_email, recipient_list, message, html_message=True, attachments=attachments)

```
**
(2) To send email using AWS SES add following settings in your settings.py**

AWS_REGION = ''

AWS_SES_ACCESS_KEY = ''

AWS_SES_SECRET_KEY = ''


```
#!python

from email_component.ses_emails import *

# To test AWS SES settings

test_connection() # this will return list of verified email address list or raise aws exception

# To send test email
source = 'aws_verified_email@example.com'
to = ['test@example.com', 'xyz@example.com']

send_test_email(source, to)

# To send email
source = 'aws_verified_email@example.com'
to = ['test@example.com', 'xyz@example.com']
subject = 'subject'
body = '<html><body>My HTML Email Message</body></html>'
format = 'html'

send_ses_email(source, subject, body, to, format=format)

# To send email with attachments
source = 'aws_verified_email@example.com'
to = ['test@example.com', 'xyz@example.com']
subject = 'subject'
body = '<html><body>My HTML Email Message</body></html>'
format = 'html'
attachments = ['/path/to/file1', '/path/to/file2']

send_ses_email(source, subject, body, to, format=format, attachments=attachments)

```


**(3) To send sms using Dove sms api add following settings in settings.py**

DOVE_USER = ''

DOVE_KEY = ''


```
#!python

from sms_component.dovesms import send_sms

msg = "My SMS Content"
mobile = "9898989898"

send_sms(msg, mobile)
```

**(4) To send sms using Twilio sms api add following settings in settings.py**

ACCOUNT_SID = ''

AUTH_TOKEN = ''

FROM = ''
```
#!python

from sms_component.twiliosms import send_sms

msg = "My SMS Content"
mobile = "9898989898"

send_sms(msg, mobile)
```

**(5) To send sms using Mvaayoo sms api add following settings in settings.py**

MVAAYOO_USER = ''

MVAAYOO_PASS = ''

MVAYOO_SENDER_ID = ''
```
#!python

from sms_component.mvaayoosms import send_sms

msg = "My SMS Content"
mobile = "9898989898"

send_sms(msg, mobile)
```